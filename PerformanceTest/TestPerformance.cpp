#include <benchmark/benchmark.h>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include <spdlog/spdlog.h>

#include <unsupported/Eigen/SparseExtra>

// Level 1 BLAS routines
void xSCAL(benchmark::State &state) {
  const auto size = state.range(0);
  double a = 2.0;
  Eigen::VectorXd x = Eigen::VectorXd::Random(size);
  for (auto s : state) {
    x = a * x;
    benchmark::DoNotOptimize(x);
  }
}

void xAXPY(benchmark::State &state) {
  const auto size = state.range(0);
  double a = 2.0;
  Eigen::VectorXd y = Eigen::VectorXd::Random(size);
  Eigen::VectorXd x = Eigen::VectorXd::Random(size);
  for (auto s : state) {
    y = a * x + y;
    benchmark::DoNotOptimize(y);
  }
}

void xDOT(benchmark::State &state) {

  const auto size = state.range(0);
  Eigen::VectorXd y = Eigen::VectorXd::Random(size);
  Eigen::VectorXd x = Eigen::VectorXd::Random(size);
  double result;
  for (auto s : state) {
    result = x.dot(y);
    benchmark::DoNotOptimize(result);
  }
}

#ifdef EIGEN_USE_BLAS
void xDOT_openBLAS(benchmark::State &state) {

  const auto size = state.range(0);
  Eigen::VectorXd y = Eigen::VectorXd::Random(size);
  Eigen::VectorXd x = Eigen::VectorXd::Random(size);
  double result;
  int one = 1;
  int rows = x.rows();
  for (auto s : state) {
    result = ddot_(&rows, x.data(), &one, y.data(), &one);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(xDOT_openBLAS)->RangeMultiplier(2)->Range(8, 2097152);
#endif

template <typename T> void xDOT(benchmark::State &state, T) {
  T y = T::Random();
  T x = T::Random();
  double result;
  for (auto s : state) {
    benchmark::DoNotOptimize(result = x.dot(y));
  }
}

void xNRM2(benchmark::State &state) {
  const auto size = state.range(0);
  Eigen::VectorXd x = Eigen::VectorXd::Random(size);
  double result;
  for (auto s : state) {
    benchmark::DoNotOptimize(result = x.norm());
  }
}

void xASUM(benchmark::State &state) {
  const auto size = state.range(0);
  Eigen::VectorXd x = Eigen::VectorXd::Random(size);
  double result;
  for (auto s : state) {
    benchmark::DoNotOptimize(result = x.sum());
  }
}

BENCHMARK(xSCAL)->RangeMultiplier(2)->Range(8, 2097152);
BENCHMARK(xAXPY)->RangeMultiplier(2)->Range(8, 2097152);
BENCHMARK(xDOT)->RangeMultiplier(2)->Range(8, 2097152);
BENCHMARK_CAPTURE(xDOT, 2d, Eigen::Matrix<double, 2, 1>());
BENCHMARK_CAPTURE(xDOT, 3d, Eigen::Matrix<double, 3, 1>());
BENCHMARK_CAPTURE(xDOT, 4d, Eigen::Matrix<double, 4, 1>());
BENCHMARK_CAPTURE(xDOT, 6d, Eigen::Matrix<double, 6, 1>());
BENCHMARK_CAPTURE(xDOT, 9d, Eigen::Matrix<double, 9, 1>());
BENCHMARK_CAPTURE(xDOT, 12d, Eigen::Matrix<double, 12, 1>());
BENCHMARK_CAPTURE(xDOT, 24d, Eigen::Matrix<double, 24, 1>());
BENCHMARK(xNRM2)->RangeMultiplier(2)->Range(8, 2097152);
BENCHMARK(xASUM)->RangeMultiplier(2)->Range(8, 2097152);

// Level 2 BLAS routines
void xGEMV(benchmark::State &state, bool transpose) {

  const auto m = state.range(0);
  const auto n = state.range(0);
  double a = 2.0;
  double b = 1.5;
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(m, n);
  Eigen::VectorXd x = Eigen::VectorXd::Random(n);
  Eigen::VectorXd y = Eigen::VectorXd::Random(m);
  for (auto s : state) {
    if (transpose)
      y = a * A.transpose() * x + b * y;
    else
      y = a * A * x + b * y;
    benchmark::DoNotOptimize(y);
  }
}

void xSYMV(benchmark::State &state) {

  const auto m = state.range(0);
  const auto n = state.range(0);
  double a = 2.0;
  double b = 1.5;
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(m, n);
  // Make it symmetric
  A = A.transpose() + A;
  Eigen::VectorXd x = Eigen::VectorXd::Random(n);
  Eigen::VectorXd y = Eigen::VectorXd::Random(m);
  for (auto s : state) {
    y = a * A * x + b * y;
    benchmark::DoNotOptimize(y);
  }
}

BENCHMARK_CAPTURE(xGEMV, Transpose, true)->RangeMultiplier(2)->Range(1, 16384);
BENCHMARK_CAPTURE(xGEMV, DontTranspose, true)
    ->RangeMultiplier(2)
    ->Range(1, 16384);
BENCHMARK(xSYMV)->RangeMultiplier(2)->Range(1, 16384);

void aAXpbY(benchmark::State &state) {
  const auto m = state.range(0);
  const auto n = state.range(0);
  double a = 2.0;
  double b = 1.5;
  Eigen::MatrixXd A = Eigen::MatrixXd::Random(m, n);
  Eigen::VectorXd x = Eigen::VectorXd::Random(n);
  Eigen::VectorXd y = Eigen::VectorXd::Random(m);
  for (auto s : state) {
    y = a * A * (x + b * y);
    benchmark::DoNotOptimize(y);
  }
}
BENCHMARK(aAXpbY)->RangeMultiplier(2)->Range(1, 16384);

void cg(benchmark::State &state, const std::string &file) {
  Eigen::SparseMatrix<double> A;
  Eigen::loadMarket(A, file);

  Eigen::ConjugateGradient<decltype(A)> cg(A);
  cg.setTolerance(1e-5);
  cg.setMaxIterations(10);
  Eigen::VectorXd b;
  b.resize(A.rows());
  b.setOnes();
  Eigen::VectorXd x;
  x.resize(A.rows());
  x.setZero();
  for (auto s : state) {
    x = cg.solve(b);
    benchmark::DoNotOptimize(x);
  }
}

const auto smallMatrixpath = MATRICES_DATA_DIR "/M19101.mtx";
const auto mediumMatrixpath = MATRICES_DATA_DIR "/M75546.mtx";
const auto bigMatrixpath = MATRICES_DATA_DIR "/M1880193.mtx";

BENCHMARK_CAPTURE(cg, M19101, smallMatrixpath);
BENCHMARK_CAPTURE(cg, M75546, mediumMatrixpath);
BENCHMARK_CAPTURE(cg, M1880193, bigMatrixpath);

void cg_UpperLower(benchmark::State &state, const std::string &file) {
  Eigen::SparseMatrix<double> A;
  Eigen::loadMarket(A, file);

  Eigen::ConjugateGradient<decltype(A), Eigen::Lower | Eigen::Upper> cg(A);
  cg.setTolerance(1e-5);
  cg.setMaxIterations(10);
  Eigen::VectorXd b;
  b.resize(A.rows());
  b.setOnes();
  Eigen::VectorXd x;
  x.resize(A.rows());
  x.setZero();
  for (auto s : state) {
    x = cg.solve(b);
    benchmark::DoNotOptimize(x);
  }
}
BENCHMARK_CAPTURE(cg_UpperLower, M19101, smallMatrixpath);
BENCHMARK_CAPTURE(cg_UpperLower, M75546, mediumMatrixpath);
BENCHMARK_CAPTURE(cg_UpperLower, M1880193, bigMatrixpath);

void cg_IdentityPrecond(benchmark::State &state, const std::string &file) {
  Eigen::SparseMatrix<double> A;
  Eigen::loadMarket(A, file);

  Eigen::ConjugateGradient<decltype(A), Eigen::Lower,
                           Eigen::IdentityPreconditioner>
      cg(A);
  cg.setTolerance(1e-5);
  cg.setMaxIterations(10);
  Eigen::VectorXd b;
  b.resize(A.rows());
  b.setOnes();
  Eigen::VectorXd x;
  x.resize(A.rows());
  x.setZero();
  for (auto s : state) {
    x = cg.solve(b);
    benchmark::DoNotOptimize(x);
  }
}
BENCHMARK_CAPTURE(cg_IdentityPrecond, M19101, smallMatrixpath);
BENCHMARK_CAPTURE(cg_IdentityPrecond, M75546, mediumMatrixpath);
BENCHMARK_CAPTURE(cg_IdentityPrecond, M1880193, bigMatrixpath);

void cg_UpperLower_IdentityPrecond(benchmark::State &state,
                                   const std::string &file) {
  Eigen::SparseMatrix<double> A;
  Eigen::loadMarket(A, file);

  Eigen::ConjugateGradient<decltype(A), Eigen::Lower | Eigen::Upper,
                           Eigen::IdentityPreconditioner>
      cg(A);
  cg.setTolerance(1e-5);
  cg.setMaxIterations(10);
  Eigen::VectorXd b;
  b.resize(A.rows());
  b.setOnes();
  Eigen::VectorXd x;
  x.resize(A.rows());
  x.setZero();
  for (auto s : state) {
    x = cg.solve(b);
    benchmark::DoNotOptimize(x);
  }
}
BENCHMARK_CAPTURE(cg_UpperLower_IdentityPrecond, M19101, smallMatrixpath);
BENCHMARK_CAPTURE(cg_UpperLower_IdentityPrecond, M75546, mediumMatrixpath);
BENCHMARK_CAPTURE(cg_UpperLower_IdentityPrecond, M1880193, bigMatrixpath);

BENCHMARK_MAIN();
