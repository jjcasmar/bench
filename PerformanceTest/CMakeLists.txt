set(SOURCES
        TestPerformance.cpp
        )

set(HEADERS
	)

add_compile_options(-march=native)
add_executable(PerformanceTest ${SOURCES} ${HEADERS})
target_link_libraries(PerformanceTest Eigen3::Eigen benchmark::benchmark)

target_compile_definitions(PerformanceTest PRIVATE MATRICES_DATA_DIR="${CMAKE_CURRENT_SOURCE_DIR}/matrices/")

if (EIGEN_USE_BLAS)
    target_compile_definitions(PerformanceTest PRIVATE EIGEN_USE_BLAS)
    target_link_libraries(PerformanceTest OpenBLAS::OpenBLAS)
endif()

if (USE_OPENMP)
    target_link_libraries(PerformanceTest OpenMP::OpenMP_CXX)
endif()
