#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>
#include <iostream>

#include <chrono>

template <typename Scalar> struct TestMatrix;

namespace Eigen {
namespace internal {
template <typename Scalar>
struct traits<TestMatrix<Scalar>>
    : Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> {};
} // namespace internal
} // namespace Eigen

// Example of a matrix-free wrapper from a user type to Eigen's compatible type
// For the sake of simplicity, this example simply wrap a Eigen::SparseMatrix.
template <typename Scalar>
struct TestMatrix : Eigen::EigenBase<TestMatrix<Scalar>> {
  // Required typedefs, constants, and method:
  typedef int StorageIndex;
  enum {
    RowsAtCompileTime = Eigen::Dynamic,
    ColsAtCompileTime = Eigen::Dynamic,
    MaxColsAtCompileTime = Eigen::Dynamic,
    MaxRowsAtCompileTime = Eigen::Dynamic,
    IsRowMajor = false
  };
  Eigen::Index rows() const { return m_spMat.rows(); }
  Eigen::Index cols() const { return m_spMat.cols(); }
  template <typename Rhs>
  Eigen::Product<TestMatrix, Rhs, Eigen::DefaultProduct>
  operator*(const Eigen::MatrixBase<Rhs> &x) const {
    return Eigen::Product<TestMatrix, Rhs, Eigen::DefaultProduct>(*this,
                                                                  x.derived());
  }

  void resizeSp(int rows, int cols) { m_spMat.resize(rows, cols); }
  void resizeDense(int rows, int cols) { m_denseMat.resize(rows, cols); }

  // Custom API:
  TestMatrix() {}

  Eigen::SparseMatrix<Scalar> m_spMat;
  Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m_denseMat;
  std::vector<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> m_denses;
};

// Implementation of MatrixReplacement * Eigen::DenseVector though a
// specialization of internal::generic_product_impl:
namespace Eigen {
namespace internal {
template <typename Scalar,
          typename Rhs>
struct generic_product_impl<TestMatrix<Scalar>,     //
                            Rhs,                    //
                            DenseShape, DenseShape, //
                            GemvProduct>
    : generic_product_impl_base<TestMatrix<Scalar>, Rhs,
                                generic_product_impl<TestMatrix<Scalar>, Rhs>> {
  using Mat = TestMatrix<Scalar>;
  template <typename Dest>
  static void scaleAndAddTo(Dest &dst, const Mat &lhs, const Rhs &rhs,
                            const Scalar &alpha) {
    auto op = lhs.m_denses[0] * rhs;
    for (int i = 1; i < 10; ++i) {
      op += lhs.m_denses[i] * rhs;
    }
    auto result =
        lhs.m_spMat * rhs + lhs.m_denseMat.transpose() * (lhs.m_denseMat * rhs);
    dst.noalias() = result;
  }
};
} // namespace internal
} // namespace Eigen

int main() {
  int n = 10000;
  TestMatrix<double> A;
  A.resizeDense(3, n);
  A.resizeSp(n, n);

  Eigen::VectorXd b(n), x;
  for (int i = 0; i < n; ++i)
    b[i] = i;

  auto start = std::chrono::steady_clock::now();
  x = A * b;
  auto end = std::chrono::steady_clock::now();
  std::cout << "Elapsed: "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     start)
                   .count()
            << "ms\n";
}
